const tokenService = {
	token: {
		EternalTwin: false,
		MyHordes: false,
		DinoRPG: false,
		LaBrute: false,
		NeoParc: false,
		eMush: false,
		DirectQuiz: false,
		Twinoid: false,
		localhost: false
	},
	setToken(newToken: boolean, site: string): void {
		switch (site) {
			case 'EternalTwin':
				tokenService.token.EternalTwin = newToken;
				break;
			case 'MyHordes':
				tokenService.token.MyHordes = newToken;
				break;
			case 'DinoRPG':
				tokenService.token.DinoRPG = newToken;
				break;
			case 'LaBrute':
				tokenService.token.LaBrute = newToken;
				break;
			case 'NeoParc':
				tokenService.token.NeoParc = newToken;
				break;
			case 'eMush':
				tokenService.token.eMush = newToken;
				break;
			case 'DirectQuiz':
				tokenService.token.DirectQuiz = newToken;
				break;
			case 'Twinoid':
				tokenService.token.Twinoid = newToken;
				break;
			case 'localhost':
				tokenService.token.localhost = newToken;
				break;
			default:
				break;
		}
	}
};
export default tokenService;
