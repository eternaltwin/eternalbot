import { Config, getConfig, loadConfigFile } from '../utils/context';
import tokenService from './token';
loadConfigFile();
const config: Config = getConfig();

export async function postBot(message: string, status: 'up' | 'down') {
	status === 'up' ? tokenService.setToken(true, message) : tokenService.setToken(false, message);
	let webhook = `https://discord.com/api/webhooks/${config.Discord.channel}/${config.Discord.token}`;
	let params = {
		username: 'Agent Smith',
		embeds: [
			{
				title: `Website ${message} is ${status}.`,
				color: status === 'down' ? 0xff0000 : 0x00ff0d,
				thumbnail: {
					url: ''
				},
				fields: [
					{
						name: status === 'down' ? 'Administrators have acknowledge this is issue.' : '',
						value: status === 'down' ? 'An intervention is planned' : '',
						inline: true
					}
				]
			}
		]
	};
	console.log(status);
	fetch(webhook, {
		method: 'POST',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify(params)
	});
}
