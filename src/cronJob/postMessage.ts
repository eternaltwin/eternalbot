import cron, { CronJob } from 'cron';
import { serverList } from '../constant/serverList';
import { postBot } from '../business/keepalive';
import tokenService from '../business/token';

const postMessage = (): CronJob => {
	const CronJob = cron.CronJob;

	return new CronJob('*/1 * * * *', async () => {
		Object.entries(serverList).forEach(async serveur => {
			try {
				await fetch(serveur[1], { method: 'GET' });
				if (!Object.entries(tokenService.token).find(s => s[0] === serveur[0])![1]) await postBot(serveur[0], 'up');
			} catch (err) {
				if (Object.entries(tokenService.token).find(s => s[0] === serveur[0])![1]) await postBot(serveur[0], 'down');
			}
		});
	});
};

export { postMessage };
