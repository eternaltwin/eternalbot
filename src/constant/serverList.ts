export const serverList: Record<string, string> = {
	EternalTwin: 'https://eternal-twin.net/',
	MyHordes: 'https://myhordes.eu',
	DinoRPG: 'https://dinorpg.eternaltwin.org/',
	LaBrute: 'https://brute.eternaltwin.org/',
	NeoParc: 'https://neoparc.eternaltwin.org/',
	eMush: 'https://emush.eternaltwin.org/',
	DirectQuiz: 'https://www.directquiz.org/',
	Twinoid: 'https://twinoid.com/oauth/auth'
	// localhost: 'http://localhost:8080'
};
